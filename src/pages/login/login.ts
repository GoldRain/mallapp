import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
//import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';
import { PopupPage } from '../popup/popup';
import { AppserviceProvider } from '../../providers/appservice/appservice';
import { AngularFireAuth } from 'angularfire2/auth';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  lform: FormGroup;
  constructor(private afAuth: AngularFireAuth, private fb: FormBuilder,public navCtrl: NavController, public navParams: NavParams,
     private statusBar: StatusBar, public appservice: AppserviceProvider, public loadingCtrl: LoadingController,
     public alertCtrl: AlertController) {
      this.statusBar.backgroundColorByHexString('#272039');
      this.lform = this.fb.group({
        email: ['',Validators.required],
        password: ['',Validators.required],
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  login(data){
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
    this.afAuth.auth.signInWithEmailAndPassword(data.email, data.password).then((res)=>{
      loader.dismiss();
      if(res.user.uid){
        window.localStorage.setItem('uid',res.user.uid);
        this.navCtrl.push(PopupPage,{mall:this.navParams.get('mall')});
      }
    },
    error=>{
      loader.dismiss();
      const alert = this.alertCtrl.create({
        title: 'Error!',
        subTitle: error.message,
        buttons: ['OK']
      });
      alert.present();
    })

  }
  register(){
    this.navCtrl.push(RegisterPage,{mall:this.navParams.get('mall')});
  }

}
