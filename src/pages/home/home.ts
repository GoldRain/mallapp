import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Slides } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';
import { PopupPage } from '../popup/popup';
import { AppserviceProvider } from '../../providers/appservice/appservice';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Slides) slides: Slides;
  malls:any=[];
 
  constructor(public navCtrl: NavController, private statusBar: StatusBar, public appservice: AppserviceProvider,
    public loadingCtrl: LoadingController) {
  }
  ionViewDidEnter(){
    //this.navCtrl.setRoot(HomePage);
    this.statusBar.backgroundColorByHexString('#091833');
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present().then(()=>{
      this.appservice.getMallList().subscribe((res)=>{
        console.log(res);
        loader.dismiss();
        this.malls = res.malls;
      })
    });
  }
  checkLogin(mall){
    //this.navCtrl.setRoot(HomePage);
    if(window.localStorage.getItem('uid')){
      this.navCtrl.push(PopupPage,{mall:mall});
    }else{
      if(window.localStorage.getItem('registered') == 'true') this.navCtrl.push(LoginPage,{mall:mall});
      else this.navCtrl.push(RegisterPage,{mall:mall});
    }
  }
  prev(){
    console.log('prev');
    this.slides.slidePrev();
  }
  next(){
    console.log('next');
    this.slides.slideNext();
  }
  
}
