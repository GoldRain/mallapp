import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AgendaPage } from '../agenda/agenda';
/**
 * Generated class for the AgendadetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-agendadetails',
  templateUrl: 'agendadetails.html',
})
export class AgendadetailsPage {
  agendaDetails:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.agendaDetails = this.navParams.get('details');
  }
  backagenda(){
    this.navCtrl.push(AgendaPage);
  }
  ionViewDidLoad() {
    //console.log('ionViewDidLoad AgendadetailsPage');
  }

}
