import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { MallPage } from '../mall/mall';
import { AppserviceProvider } from '../../providers/appservice/appservice';


/**
 * Generated class for the PopupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-popup',
  templateUrl: 'popup.html',
})
export class PopupPage {
  image_banner:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
   public appservice: AppserviceProvider,   private statusBar: StatusBar) {
    this.image_banner = this.navParams.get('mall').image_popup;
  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#9d4b66');
  }
  closePopup(){
    let previousIndex = this.navCtrl.getPrevious().index;
    let currentIndex = this.navCtrl.getActive().index;
    this.appservice.setMallData(this.navParams.get('mall'));//{mall:this.navParams.get('mall')}
      this.navCtrl.push(MallPage).then(() => {
          this.navCtrl.remove(currentIndex);
          if(!window.localStorage.getItem('uid')){
            this.navCtrl.remove(previousIndex);
          }
      });
    // this.navCtrl.push(MallPage).then(() => {
    //   let index = this.navCtrl.length()-2;
    //   this.navCtrl.remove(index);
    // });
  }
}
