import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { AppserviceProvider } from '../../providers/appservice/appservice';

import { AgendaPage } from '../agenda/agenda';
/**
 * Generated class for the DirectoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-directory',
  templateUrl: 'directory.html',
})
export class DirectoryPage {
  directory:any;
  directoryCoppy:any;
  selectedItem:any;
  selectedItemName:any;
  floor:any;
  alpha:any=['*'];//=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  constructor(public navCtrl: NavController, public navParams: NavParams, public appservice: AppserviceProvider,
    public loadingCtrl: LoadingController) {
      //this.alpha = [];
  }

  ionViewDidEnter() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present().then(()=>{
      this.appservice.getDirectory('3').subscribe((res)=>{
        loader.dismiss();
        if(res.hasError == false){
          res.content.stores.forEach((store)=>{
            if(this.alpha.indexOf(store.name[0]) === -1) {
              this.alpha.push(store.name[0]);
              //console.log(this.alpha);
            }
          })
          this.alpha.sort();
          this.directory = res.content.stores;
          this.directoryCoppy = res.content.stores;
        }
      })
    });
  }
  selected(data){
    this.selectedItem = data;
    this.selectedItemName = this.selectedItem.name;
    this.floor = this.selectedItem.floor;
  }
  agenda(){
    this.navCtrl.push(AgendaPage);
  }
  setFilteredItems(ev) {
    let val = ev.target.value;
    this.directory = this.directoryCoppy;
    if (val && val.trim() != '') {
      this.directory = this.directory.filter((item_all) => {
        return (item_all.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  onCancel(ev){
    this.directory = this.directoryCoppy;
  }
  alphabet(data){
    if(data == '*'){
      this.directory = this.directoryCoppy;
    }
    else{
      this.directory = this.directoryCoppy;
      this.directory = this.directory.filter((item_all) => {
        return (item_all.name[0].toLowerCase().indexOf(data.toLowerCase()) > -1);
      })
    }
  }
}
