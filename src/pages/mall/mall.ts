import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { AppserviceProvider } from '../../providers/appservice/appservice';

import { OfferPage } from '../offer/offer';
import { ServicePage } from '../service/service';
import { DirectoryPage } from '../directory/directory';
import { AccountPage } from '../account/account';
import { StatusBar } from '@ionic-native/status-bar';
/**
 * Generated class for the MallPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mall',
  templateUrl: 'mall.html',
})
export class MallPage {

  offer = OfferPage;
  service = ServicePage;
  directory = DirectoryPage;
  account = AccountPage;
  rootPage = AccountPage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public appservice: AppserviceProvider,
    public loadingCtrl: LoadingController, private statusBar: StatusBar) {

  }

  ionViewDidEnter() {
    this.statusBar.backgroundColorByHexString('#7d153d');
    // const loader = this.loadingCtrl.create({
    //   content: "Please wait...",
    //   duration: 3000
    // });
    // loader.present();
    // this.appservice.getService('3').subscribe((res)=>{
    //   loader.dismiss();
    //   if(res.hasError == false){
    //     //this.services= res.content.services;
    //   }
    //   console.log(res);
    // })
  }

}
