import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { AgendaPage } from '../agenda/agenda';

import { AppserviceProvider } from '../../providers/appservice/appservice';
import { AngularFireDatabase } from '@angular/fire/database';
/**
 * Generated class for the ServicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-service',
  templateUrl: 'service.html',
})
export class ServicePage {
  services:any;
  selectedMap:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public appservice: AppserviceProvider,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidEnter() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present().then(()=>{
      this.appservice.getService('3').subscribe((res)=>{
        loader.dismiss();
        if(res.hasError == false){
          this.services= res.content.services;
        }
        console.log(res);
      })
    });
  }
  selected(data){
    this.selectedMap = data;
  }
  agenda(){
    this.navCtrl.push(AgendaPage);
  }
}
