import { Component } from '@angular/core';
import { App, NavController, NavParams, LoadingController, ActionSheetController, AlertController } from 'ionic-angular';
import { AppserviceProvider } from '../../providers/appservice/appservice';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { trigger, style, animate, transition } from '@angular/animations';
import { Camera, CameraOptions } from '@ionic-native/camera'
import { AgendaPage } from '../agenda/agenda';
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../home/home';
/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-account',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(100%)', opacity: 0}),
          animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('500ms', style({transform: 'translateX(100%)', opacity: 0}))
        ])
      ]
    )
  ],
  templateUrl: 'account.html',
})
export class AccountPage {
  rform: FormGroup;
  userData:any;
  showQRdiv:Boolean=false;
  constructor(public app: App,private afAuth: AngularFireAuth,public navCtrl: NavController, public navParams: NavParams, 
    private fb: FormBuilder, public appservice: AppserviceProvider, public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController, private camera: Camera, public alertCtrl: AlertController ) {
      // , public afDatabase: AngularFireDatabase,
      this.userData = {
        address: "",
        birthday: "",
        city: "",
        email: "",
        identity: "",
        name: "",
        phone: ""
      }
    this.rform = this.fb.group({
      name: ['', Validators.required],
      identity: ['', Validators.required ],
      email: ['',Validators.required],
      birthday: ['', Validators.required ],
      address: ['', Validators.required ],
      city: ['', Validators.required ],
      phone: ['', Validators.required ]
    });
  }
  ionViewDidEnter() {
    this.userData.profilepicUrl = 'assets/imgs/single-logo.png';
    this.showQRdiv = false;
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    this.appservice.query().then((res)=>{
      this.userData = res;
      this.userData.profilepicUrl = 'assets/imgs/single-logo.png';
      this.getImageURL();
      loader.dismiss();
    });
    // this.appservice.getService('3').subscribe((res)=>{
    //   loader.dismiss();
    //   if(res.hasError == false){
    //     //this.services= res.content.services;
    //   }
    //   console.log(res);
    // })
  }
  showQR(){
    this.showQRdiv = true;
  }
  hideQR(){
    this.showQRdiv = false;
  }
  agenda(){
    this.navCtrl.push(AgendaPage);
  }
  profilepic(){
    const coptions: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: 1,
      correctOrientation: true
    }
    const goptions: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: 0,
      correctOrientation: true
    }
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Update Profile Pic',
      buttons: [
        {
          text: 'Camera',
          handler: () => {
            this.camera.getPicture(coptions).then((imageData) => {
              let base64Image = 'data:image/jpeg;base64,' + imageData;
              this.appservice.uploadImage(base64Image).then((res)=>{
                this.appservice.getImageURL().then((res)=>{
                  this.getImageURL(); 
                })
              })
             }, (err) => {
              console.log(err);
             });
          }
        },{
          text: 'Gallery',
          handler: () => {
            this.camera.getPicture(goptions).then((imageData) => {
              let base64Image = 'data:image/jpeg;base64,' + imageData;
              this.appservice.uploadImage(base64Image).then((res)=>{
               this.getImageURL();                
              })
             }, (err) => {
              console.log(err);
             });
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  getImageURL(){
    this.appservice.getImageURL().then((res)=>{
      this.userData.profilepicUrl = res;
    })
  }
  doLogout(){
    //console.log(this.navCtrl.getViews());
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
    });
    loader.present().then(()=>{
      this.afAuth.auth.signOut().then((res)=>{
        this.app.getRootNavs()[0].setRoot(HomePage);
        loader.dismiss();
        window.localStorage.removeItem('uid');
      },
      error=>{
        loader.dismiss();
        const alert = this.alertCtrl.create({
          title: 'Error!',
          subTitle: error.message,
          buttons: ['OK']
        });
        alert.present();
      })
    })
  }
}
