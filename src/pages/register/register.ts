import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController  } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppserviceProvider } from '../../providers/appservice/appservice';

import { PopupPage } from '../popup/popup';
import { LoginPage } from '../login/login';

// import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  //user : AngularFireList<any>;
  rform: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private fb: FormBuilder, public appservice: AppserviceProvider,
    public loadingCtrl: LoadingController , private statusBar: StatusBar,
    public alertCtrl: AlertController) {
      this.statusBar.backgroundColorByHexString('#272039');
      // , public afDatabase: AngularFireDatabase,
      //
    this.rform = this.fb.group({
      name: ['', Validators.required],
      identity: ['', Validators.required ],
      email: ['',Validators.required],
      password: ['',Validators.required],
      repass: ['',Validators.required],
      birthday: ['', Validators.required ],
      address: ['', Validators.required ],
      city: ['', Validators.required ],
      phone: ['', Validators.required ]
    });
    //this.user = afDatabase.list('/users').valueChanges();
  }

  register(data){
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
    this.appservice.addUser(data).then((res) => {
      loader.dismiss();
      if(res.message){
        const alert = this.alertCtrl.create({
          title: 'Error!',
          subTitle: res.message,
          buttons: ['OK']
        });
        alert.present();
      }
      else if(res.user.uid){
        const alert = this.alertCtrl.create({
          title: 'Success!',
          subTitle: 'Account created',
          buttons: [{
            text: 'OK',
            handler: () => {
              window.localStorage.setItem('uid',res.user.uid);
              window.localStorage.setItem('registered','true');
              this.navCtrl.push(PopupPage,{mall:this.navParams.get('mall')});
            }
          }]
        });
        alert.present();
      }
      //console.log(res);
      //this.navCtrl.push(PopupPage);
    })

  }
  backToLogin(){
    this.navCtrl.setRoot(LoginPage,{mall:this.navParams.get('mall')});
    //this.appservice.qwery();
  }

}
