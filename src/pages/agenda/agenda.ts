import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import {  AgendadetailsPage } from '../agendadetails/agendadetails';
import { AppserviceProvider } from '../../providers/appservice/appservice';
/**
 * Generated class for the AgendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-agenda',
  templateUrl: 'agenda.html',
})
export class AgendaPage {
  agenda:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public appservice: AppserviceProvider, public loadingCtrl: LoadingController) {
  }
  agendadetails(data){
    this.navCtrl.push(AgendadetailsPage,{details: data})
  }
  ionViewDidEnter() {
    const loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present().then(()=>{
      this.appservice.getAgenda('3').subscribe((res)=>{
        loader.dismiss();
        if(res.hasError == false){
          this.agenda= res.content.events;
        }
        console.log(res);
      })
    });
  }

}
