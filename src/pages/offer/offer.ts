import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { AppserviceProvider } from '../../providers/appservice/appservice';
import { AgendaPage } from '../agenda/agenda';
import moment from 'moment';
/**
 * Generated class for the OfferPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-offer',
  templateUrl: 'offer.html',
})
export class OfferPage {
  offers:any = [];
  mall:any;
  timer:any;
  hours:any;
  minutes:any;
  seconds:any;
  timOut:any;
  timeOver:boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, public appservice: AppserviceProvider, public loadingCtrl: LoadingController) {
      // var c = document.getElementById("myCanvas");
      // var ctx = c.getContext("2d");
      // ctx.beginPath();
      // ctx.arc(100, 75, 50, 0, 2 * Math.PI);
      // ctx.stroke();
      if(!window.localStorage.getItem('now')){
        window.localStorage.setItem('now',new Date().toString());
        this.timer = window.localStorage.getItem('now');
      }else if(window.localStorage.getItem('now')){
        let oldDate = window.localStorage.getItem('now');
        let day = moment().diff(oldDate, 'days');
        if(day >= 1){
          window.localStorage.setItem('now',new Date().toString());
          this.timer = window.localStorage.getItem('now');
        }else if(!moment().isSame(moment(window.localStorage.getItem('now')), 'day')){
          window.localStorage.setItem('now',new Date().toString());
          this.timer = window.localStorage.getItem('now');
        }
        else{
          this.timer = window.localStorage.getItem('now');
        }
      }
      this.startTimer();
  }
  startTimer(){
    let diff = moment(moment(this.timer)).diff(new Date());
    //console.log(moment.duration(diff)['_data']);
    if(moment.duration(diff)['_data'].hours == 0){
      this.timeOver = false;
      this.hours =  '00';//Math.floor(d.asHours());
      this.minutes = (59 + parseInt(moment.duration(diff)['_data'].minutes));
      this.seconds = (59 + parseInt(moment.duration(diff)['_data'].seconds));
      if(this.minutes<10) this.minutes = '0'+this.minutes;
      if(this.seconds <10) this.seconds = '0'+this.seconds;
    }else if(moment.duration(diff)['_data'].hours < 0){
      this.timeOver = true;
      this.hours = '00';
      this.minutes = '00';
      this.seconds = '00';
    }

    this.timOut = setTimeout(()=>{
      this.startTimer();
    },1000)
  }
  agenda(){
    //alert('hi')
    this.navCtrl.push(AgendaPage)
  }
  ionViewDidEnter() {
    this.offers = [];
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present().then(()=>{
      //loader.dismiss();
      this.appservice.getOffer('3').subscribe((res)=>{
        loader.dismiss();
        for (var i = 0; i < res.promotions.length; i++) {
          res.promotions[i].showQR = false
          this.offers.push(res.promotions[i]);
          if(i == res.promotions.length-1) console.log(this.offers);
        }
        //this.offers = res.promotions;

        if(res.hasError == false){

        }
        console.log(res);
      })
    });
  }
  checkOffer(data){
    if(!this.timeOver){
      let itemIndex = this.offers.findIndex(item => item.id_product == data.id_product);
      this.offers[itemIndex].showQR = true;
      console.log(this.offers);
    }
  }
}
