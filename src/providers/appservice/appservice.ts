//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireDatabase, AngularFireList  } from '@angular/fire/database';
import { AngularFireAuth } from "angularfire2/auth";
import { query } from '@angular/core/src/animation/dsl';
import * as firebase from 'firebase/app';
import 'firebase/storage';
/*
  Generated class for the AppserviceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AppserviceProvider {
  private baseUrl:string= 'http://circulomall.ecomm360.net/prestashop/modules/circulomall/api.php?type=';
  private offerUrl:string = 'http://circulomall.ecomm360.net/prestashop/modules/PromoConfigurator/api/';
  private mallListUrl:string = 'http://circulomall.ecomm360.net/prestashop/modules/circulomall/requests/malls.php';
  companiesRef:AngularFireList<any>;
  mallData:any;
  constructor(private afAuth: AngularFireAuth,public http: Http, public db: AngularFireDatabase) {
    //console.log('Hello AppserviceProvider Provider');

  }
  setMallData(data){
    // console.log(data);
    this.mallData = data;
  }
  getMallList(){
    return this.http.get(this.mallListUrl).map(res=>res.json());
  }
  getDirectory(id){
    return this.http.get(this.baseUrl+'store&mall='+this.mallData.id).map(res=>res.json());
  }
  getOffer(id){
    // console.log(this.mallData);
    return this.http.get(this.offerUrl).map(res=>res.json());
  }
  getService(id){
    return this.http.get(this.baseUrl+'service&mall='+this.mallData.id).map(res=>res.json());
  }
  getAgenda(id){
    return this.http.get(this.baseUrl+'event&mall='+this.mallData.id).map(res=>res.json());
  }
  addUser(data){
    return new Promise<any>((resolve, reject) => {
     this.afAuth.auth.createUserWithEmailAndPassword(data.email, data.password)
      .then(
        (res) => {
          // console.log(res.user);
          // console.log(res.user.uid);
          const ref = this.db.object(`/users/${res.user.uid}`);
          ref.set({
            'name': data.name,
            'identity' : data.identity,
            'email':data.email,
            'birthday':data.birthday,
            'address':data.address,
            'city':data.city,
            'phone': data.phone,
            'id': res.user.uid
          });
          //console.log(ref);
          resolve(res)
        },
        (err) => {
          //console.log(err);
          resolve(err)
        }
      )
    })
  }
  query(){
    return new Promise<any>((resolve, reject) => {
      this.db.database.ref('/users')
      .orderByChild('id').equalTo(window.localStorage.getItem('uid')).on("child_added", function(snapshot){
        resolve(snapshot.val())
      },function(error){
        reject(error)
      })
    })
  }
  uploadImage(imageURI){
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('userProfile').child(window.localStorage.getItem('uid'));
      this.encodeImageUri(imageURI, function(image64){
        imageRef.putString(image64, 'data_url')
        .then(snapshot => {
          resolve(snapshot)
        }, err => {
          reject(err);
        })
      })
    })
  }
  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux:any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL("image/jpeg");
      callback(dataURL);
    };
    img.src = imageUri;
  }
  getImageURL(){
    return new Promise<any>((resolve, reject) => {
      let imgUrl: string;
      try{
        let storageRef = firebase.storage().ref();
        storageRef.child("/userProfile/" + window.localStorage.getItem('uid') ).getDownloadURL().then(function(url){
          resolve(url)
        });
      }
      catch(e){
        reject(e);
      }
    })  
  }
}
