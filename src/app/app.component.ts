import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { MallPage } from '../pages/mall/mall';
// import { RegisterPage } from '../pages/register/register';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  //rootPage:any = MallPage;
  rootPage:any = HomePage;
  
  constructor(private platform: Platform, private statusBar: StatusBar, private splashScreen: SplashScreen) {
    platform.ready().then(() => {
      if (this.platform.is('cordova')) {
        this.statusBar.overlaysWebView(false);
        this.statusBar.styleBlackTranslucent();
        this.statusBar.backgroundColorByHexString('#091833');
        //this.navCtrl.setRoot(HomePage);
        //this.statusBar.styleDefault();
        // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        setTimeout(()=>{
          this.splashScreen.hide();
        },100);
      }
    });
  }
}
