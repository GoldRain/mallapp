import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {RoundProgressModule} from 'angular-svg-round-progressbar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { PopupPage } from '../pages/popup/popup';
import { MallPage } from '../pages/mall/mall';
import { AppserviceProvider } from '../providers/appservice/appservice';
import { OfferPage } from '../pages/offer/offer';
import { ServicePage } from '../pages/service/service';
import { DirectoryPage } from '../pages/directory/directory';
import { AccountPage } from '../pages/account/account';
import { AgendaPage } from '../pages/agenda/agenda';
import { AgendadetailsPage } from '../pages/agendadetails/agendadetails';

import { MomentModule } from 'angular2-moment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorageModule } from 'angularfire2/storage';
// import { AngularFireDatabaseModule } from 'angularfire2/database';
//import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuthModule }from 'angularfire2/auth';
export const firebaseConfig ={
  apiKey: "AIzaSyCMexkWVpQjspRQXb3T0w0oGa-qqx6QENw",
  authDomain: "database-4e14b.firebaseapp.com",
  databaseURL: "https://database-4e14b.firebaseio.com",
  projectId: "database-4e14b",
  storageBucket: "database-4e14b.appspot.com",
  messagingSenderId: "877008232936"
}
import * as firebase from "firebase";
firebase.initializeApp(firebaseConfig);
import { Camera } from '@ionic-native/camera';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    PopupPage,
    MallPage,
    OfferPage,
    ServicePage,
    DirectoryPage,
    AccountPage,
    AgendaPage,
    AgendadetailsPage,
  ],
  imports: [
    RoundProgressModule,
    BrowserAnimationsModule,
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    MomentModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(firebaseConfig),
    // AngularFireDatabaseModule,
    //AngularFirestore
    AngularFireStorageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    PopupPage,
    MallPage,
    OfferPage,
    ServicePage,
    DirectoryPage,
    AccountPage,
    AgendaPage,
    AgendadetailsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AppserviceProvider,
    AngularFireDatabase
  ]
})
export class AppModule {}
